package com.softeng.IndividualProject.service;

import com.softeng.IndividualProject.model.LifestyleActivity;
import com.softeng.IndividualProject.repository.ActivityRepo;
import com.softeng.IndividualProject.repository.LifestyleActivityRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class LifestyleActivityServiceImpl implements LifestyleActivityService {
    @Autowired
    LifestyleActivityRepo lifestyleActivityRepo;

    @Autowired
    ActivityRepo activityRepo;

    @Override
    public List<LifestyleActivity> getAllLifestyleActivity() {
        return lifestyleActivityRepo.findAllByOrderByIdAsc();
    }

    @Override
    public void addLifestyleActivity(LifestyleActivity lifestyleActivity) {
        lifestyleActivityRepo.save(lifestyleActivity);
        activityRepo.save(lifestyleActivity);
    }

    @Override
    public LifestyleActivity getLifestyleActivityById(long id) {
        if (!lifestyleActivityRepo.existsById(id)) {
            throw new EntityNotFoundException();
        }
        return lifestyleActivityRepo.findById(id);
    }

    @Override
    public void updateLifestyleActivity(LifestyleActivity lifestyleActivity) {
        lifestyleActivity.setId(lifestyleActivity.getId());
        if (!lifestyleActivityRepo.existsById(lifestyleActivity.getId())) {
            throw new EntityNotFoundException();
        }
        lifestyleActivityRepo.save(lifestyleActivity);
        activityRepo.save(lifestyleActivity);
    }
}
