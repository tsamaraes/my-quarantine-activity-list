package com.softeng.IndividualProject.service;

import com.softeng.IndividualProject.model.Activity;
import com.softeng.IndividualProject.repository.ActivityRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
public class ActivityServiceImpl implements ActivityService {

    @Autowired
    ActivityRepo activityRepo;

    @Override
    public Activity getActivityById(long id) {
       return activityRepo.findById(id);
    }

    @Override
    public void markThisActivity(Activity activity, String status, int statusId) {
        if (!activityRepo.existsById(activity.getId())) {
            throw new EntityNotFoundException();
        } else {
            if (activity.getStatus().equals(status)) {
                activity.setStatus("");
                activity.setStatusId(2);
                activityRepo.save(activity);
            } else {
                activity.setStatus(status);
                activity.setStatusId(statusId);
                activityRepo.save(activity);
            }
        }
    }
}
