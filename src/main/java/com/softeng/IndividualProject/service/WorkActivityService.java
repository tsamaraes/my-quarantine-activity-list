package com.softeng.IndividualProject.service;

import com.softeng.IndividualProject.model.WorkActivity;
import org.hibernate.jdbc.Work;

import java.util.List;

public interface WorkActivityService {
    List<WorkActivity> getAllWorkActivity();
    void addWorkActivity(WorkActivity workActivity);
    WorkActivity getWorkActivityById(long id);
    void updateWorkActivity(WorkActivity workActivity);

}
