package com.softeng.IndividualProject.service;


import com.softeng.IndividualProject.model.Activity;

public interface ActivityService {
    Activity getActivityById(long id);
    void markThisActivity(Activity activity, String status, int statusId);
}
