package com.softeng.IndividualProject.service;

import com.softeng.IndividualProject.model.LifestyleActivity;

import java.util.List;

public interface LifestyleActivityService {
    List<LifestyleActivity> getAllLifestyleActivity();
    void addLifestyleActivity(LifestyleActivity lifestyleActivity);
    LifestyleActivity getLifestyleActivityById(long id);
    void updateLifestyleActivity(LifestyleActivity lifestyleActivity);
}
