package com.softeng.IndividualProject.service;

import com.softeng.IndividualProject.model.WorkActivity;
import com.softeng.IndividualProject.repository.ActivityRepo;
import com.softeng.IndividualProject.repository.WorkActivityRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class WorkActivityServiceImpl implements WorkActivityService {

    @Autowired
    WorkActivityRepo workActivityRepo;

    @Autowired
    ActivityRepo activityRepo;

    @Override
    public List<WorkActivity> getAllWorkActivity() {
        return workActivityRepo.findAllByOrderByDueDateAsc();
    }

    @Override
    public void addWorkActivity(WorkActivity workActivity) {
        workActivityRepo.save(workActivity);
        activityRepo.save(workActivity);
    }

    @Override
    public WorkActivity getWorkActivityById(long id) {
        if (!workActivityRepo.existsById(id)) {
            throw new EntityNotFoundException();
        }
        return workActivityRepo.findById(id);
    }

    @Override
    public void updateWorkActivity(WorkActivity workActivity) {
        if (!workActivityRepo.existsById(workActivity.getId())) {
            throw new EntityNotFoundException();
        }
        workActivityRepo.save(workActivity);
        activityRepo.save(workActivity);
    }
}
