package com.softeng.IndividualProject.controller;

import com.softeng.IndividualProject.model.Activity;
import com.softeng.IndividualProject.model.LifestyleActivity;
import com.softeng.IndividualProject.repository.LifestyleActivityRepo;
import com.softeng.IndividualProject.service.ActivityService;
import com.softeng.IndividualProject.service.LifestyleActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class LifestyleActivityController {
    @Autowired
    LifestyleActivityRepo lifestyleActivityRepo;

    @Autowired
    LifestyleActivityService lifestyleActivityService;

    @Autowired
    ActivityService activityService;

    private static final String LIFESTYLEACTIVITY = "LIFESTYLEACTIVITY";

    @GetMapping("/see-lifestyle-activities")
    public String showAllLifestyleActivity(Model model) {
        model.addAttribute(LIFESTYLEACTIVITY, lifestyleActivityService.getAllLifestyleActivity());
        return "see-lifestyle-activity";
    }

    @GetMapping("/add/lifestyle-activity")
    public String showAddLifestyleActivityForm(LifestyleActivity lifestyleActivity) {
        return "add-lifestyle-activity";
    }

    @PostMapping("/add-lifestyle-activity")
    public String addLifestyleActivity(@Valid LifestyleActivity lifestyleActivity, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-lifestyle-activity";
        }
        lifestyleActivityService.addLifestyleActivity(lifestyleActivity);
        model.addAttribute(LIFESTYLEACTIVITY, lifestyleActivityService.getAllLifestyleActivity());
        return "redirect:/see-lifestyle-activities";
    }

    @GetMapping("/edit-lifestyle-activity/{id}")
    public String showEditWorkActivityForm(@PathVariable("id") long id, Model model) throws Exception {
        if(!lifestyleActivityRepo.existsById(id)) {
            throw new IllegalArgumentException("Invalid activity id " + id);
        }
        LifestyleActivity lifestyleActivity = lifestyleActivityRepo.findById(id);
        model.addAttribute("lifestyleActivity", lifestyleActivity);
        return "edit-lifestyle-activity";
    }

    @PostMapping("/update-lifestyle-activity/{id}")
    public String updateCourse(@PathVariable("id") long id, @Valid LifestyleActivity lifestyleActivity,
                               BindingResult result, Model model) {
        if (result.hasErrors()) {
            lifestyleActivity.setId(id);
            return "edit-lifestyle-activity";
        }
        lifestyleActivityService.updateLifestyleActivity(lifestyleActivity);
        model.addAttribute(LIFESTYLEACTIVITY, lifestyleActivityService.getAllLifestyleActivity());
        return "redirect:/see-lifestyle-activities";
    }

    @GetMapping("/mark-lifestyle-activity-as-finished/{id}")
    public String markLifestyleActivityAsFinished(@PathVariable("id") long id, @Valid LifestyleActivity lifestyleActivity,
                                             BindingResult result, Model model) {
        Activity la = activityService.getActivityById(id);
        activityService.markThisActivity(la, "FINISHED", 3);
        model.addAttribute(LIFESTYLEACTIVITY, lifestyleActivityService.getAllLifestyleActivity());
        return "redirect:/see-lifestyle-activities";
    }

    @GetMapping("/mark-lifestyle-activity-as-important/{id}")
    public String markLifestyleActivityAsImportant(@PathVariable("id") long id, @Valid LifestyleActivity lifestyleActivity,
                                              BindingResult result, Model model) {
        Activity la = activityService.getActivityById(id);
        activityService.markThisActivity(la, "IMPORTANT", 1);
        model.addAttribute(LIFESTYLEACTIVITY, lifestyleActivityService.getAllLifestyleActivity());
        return "redirect:/see-lifestyle-activities";
    }
}
