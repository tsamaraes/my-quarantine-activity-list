package com.softeng.IndividualProject.controller;

import com.softeng.IndividualProject.model.Activity;
import com.softeng.IndividualProject.model.WorkActivity;
import com.softeng.IndividualProject.repository.WorkActivityRepo;
import com.softeng.IndividualProject.service.ActivityService;
import com.softeng.IndividualProject.service.WorkActivityService;
import org.hibernate.jdbc.Work;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class WorkActivityController {

    @Autowired
    WorkActivityService workActivityService;

    @Autowired
    WorkActivityRepo workActivityRepo;

    @Autowired
    ActivityService activityService;

    private static final String WORKACTIVITY = "WORKACTIVITY";

    @GetMapping("/")
    public String showHomepage() {
        return "index";
    }

    @GetMapping("/see-work-activities")
    public String showAllWorkActivity(Model model) {
        model.addAttribute(WORKACTIVITY, workActivityService.getAllWorkActivity());
        return "see-work-activity";
    }

    @GetMapping("/add/work-activity")
    public String showAddWorkActivityForm(WorkActivity workActivity) {
        return "add-work-activity";
    }

    @PostMapping("/add-work-activity")
    public String addWorkActivity(@Valid WorkActivity workActivity, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-work-activity";
        }
        workActivityService.addWorkActivity(workActivity);
        model.addAttribute(WORKACTIVITY, workActivityService.getAllWorkActivity());
        return "redirect:/see-work-activities";
    }

    @GetMapping("/edit-work-activity/{id}")
    public String showEditWorkActivityForm(@PathVariable("id") long id, Model model) {
        if(!workActivityRepo.existsById(id)) {
            throw new IllegalArgumentException("Invalid activity id " + id);
        }
        WorkActivity workActivity = workActivityRepo.findById(id);
        model.addAttribute("workActivity", workActivity);
        return "edit-work-activity";
    }

    @PostMapping("/update-work-activity/{id}")
    public String updateCourse(@PathVariable("id") long id, @Valid WorkActivity workActivity,
                               BindingResult result, Model model) {
        if (result.hasErrors()) {
            workActivity.setId(id);
            return "edit-work-activity";
        }
        workActivityService.updateWorkActivity(workActivity);
        model.addAttribute(WORKACTIVITY, workActivityService.getAllWorkActivity());
        return "redirect:/see-work-activities";
    }
    @GetMapping("/mark-work-activity-as-finished/{id}")
    public String markWorkActivityAsFinished(@PathVariable("id") long id, @Valid WorkActivity workActivity,
                                             BindingResult result, Model model) {
        Activity wa = activityService.getActivityById(id);
        activityService.markThisActivity(wa, "FINISHED", 3);
        model.addAttribute(WORKACTIVITY, workActivityService.getAllWorkActivity());
        return "redirect:/see-work-activities";
    }

    @GetMapping("/mark-work-activity-as-important/{id}")
    public String markWorkActivityAsImportant(@PathVariable("id") long id, @Valid WorkActivity workActivity,
                                             BindingResult result, Model model) {
        Activity wa = activityService.getActivityById(id);
        activityService.markThisActivity(wa, "IMPORTANT", 1);
        model.addAttribute(WORKACTIVITY, workActivityService.getAllWorkActivity());
        return "redirect:/see-work-activities";
    }
}
