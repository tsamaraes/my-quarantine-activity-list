package com.softeng.IndividualProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoftwareEngineeringIndividualProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoftwareEngineeringIndividualProjectApplication.class, args);
	}

}
