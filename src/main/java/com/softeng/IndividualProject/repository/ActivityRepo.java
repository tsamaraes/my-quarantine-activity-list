package com.softeng.IndividualProject.repository;

import com.softeng.IndividualProject.model.Activity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActivityRepo extends CrudRepository<Activity, Long> {
    List<Activity> findAllByOrderByIdAsc();
    Activity findById(long id);
}
