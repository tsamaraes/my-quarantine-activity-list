package com.softeng.IndividualProject.repository;

import com.softeng.IndividualProject.model.WorkActivity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WorkActivityRepo extends CrudRepository<WorkActivity, Long> {
    @Query("FROM WorkActivity ORDER BY statusId ASC, dueDate ASC")
    List<WorkActivity> findAllByOrderByDueDateAsc();
    WorkActivity findById(long id);
}
