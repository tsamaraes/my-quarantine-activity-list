package com.softeng.IndividualProject.repository;

import com.softeng.IndividualProject.model.LifestyleActivity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LifestyleActivityRepo extends CrudRepository<LifestyleActivity, Long> {
    @Query("FROM LifestyleActivity ORDER BY statusId ASC")
    List<LifestyleActivity> findAllByOrderByIdAsc();
    LifestyleActivity findById(long id);
}
