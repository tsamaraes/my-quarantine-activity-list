package com.softeng.IndividualProject.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Getter
@Setter
@Table(name="LifestyleActivity")
public class LifestyleActivity extends Activity {
    @NotBlank(message = "Please fill in activity URL")
    private String activityURL;
}
