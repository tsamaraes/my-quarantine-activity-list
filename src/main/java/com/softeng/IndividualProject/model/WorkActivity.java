package com.softeng.IndividualProject.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Bean;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@Setter
@Table(name="WorkActivity")
public class WorkActivity extends Activity{

    @NotNull(message = "Please fill in due date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dueDate;

    @NotNull(message = "Please fill in due time")
    @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
    private LocalTime dueTime;
}
