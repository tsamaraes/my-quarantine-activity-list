package com.softeng.IndividualProject.ControllerTest;

import com.softeng.IndividualProject.model.LifestyleActivity;
import com.softeng.IndividualProject.repository.LifestyleActivityRepo;
import com.softeng.IndividualProject.service.LifestyleActivityService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc
public class LifestyleActivityControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Mock
    private BindingResult bindingResult;

    @Mock
    private Model model;

    @Mock
    LifestyleActivityRepo lifestyleActivityRepo;

    @Autowired
    LifestyleActivityService lifestyleActivityService;

    private static LifestyleActivity lifestyleActivity = new LifestyleActivity();
    private static String activityName = "Workout";
    private static String activityURL = "https://www.youtube.com/ChloesAddiction";

    @BeforeEach
    void setUpS() {
        lifestyleActivity.setId(123);
        lifestyleActivity.setActivityName(activityName);
        lifestyleActivity.setActivityURL(activityURL);
        lifestyleActivityService.addLifestyleActivity(lifestyleActivity);
    }

    @Test
    void testSeeLifestyleActivityPage() throws Exception {
        this.mockMvc.perform(get("/see-lifestyle-activities"))
                .andExpect(status().isOk())
                .andExpect(view().name("see-lifestyle-activity"));
    }

    @Test
    void testShowLifestyleActivityForm() throws Exception {
        this.mockMvc.perform(get("/add/lifestyle-activity"))
                .andExpect(status().isOk())
                .andExpect(view().name("add-lifestyle-activity"));
    }

    @Test
    void testIfAddLifestyleActivityErrorThenStayAtFormAndAddValidLifestyleActivity() throws Exception {
        lifestyleActivity.setActivityName(null);
        this.mockMvc.perform(post("/add-lifestyle-activity")
                .flashAttr("lifestyleActivity", lifestyleActivity)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(view().name("add-lifestyle-activity"));

        lifestyleActivity.setActivityName("Watch movies");
        this.mockMvc.perform(post("/add-lifestyle-activity")
                .flashAttr("lifestyleActivity", lifestyleActivity)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(view().name("redirect:/see-lifestyle-activities"));
    }

    @Test
    void checkIfShowUpdateLifestyleActivityFormIfIdNotFoundThenThrowException() throws Exception{
        try {
            mockMvc.perform(get("/edit-lifestyle-activity/0"))
                    .andExpect(view().name("edit-lifestyle-activity"));
        } catch (Exception e) {
            assertEquals("Request processing failed; nested exception is java.lang.IllegalArgumentException: Invalid activity id 0", e.getMessage());
        }
    }

    @Test
    void checkIfShowUpdateLifestyleActivityFormWithValidIdThenShowTemplate() throws Exception{
            mockMvc.perform(get("/edit-lifestyle-activity/" + Long.toString(lifestyleActivityService.getAllLifestyleActivity().get(0).getId()))
                    .flashAttr("lifestyleActivity", lifestyleActivity)
                    .flashAttr("result", bindingResult)
                    .flashAttr("model", model))
                    .andExpect(view().name("edit-lifestyle-activity"));
    }

    @Test
    public void testUpdateLifestyleActivityByIdValid() throws Exception {
        this.mockMvc.perform(post("/update-lifestyle-activity/" + Long.toString(lifestyleActivityService.getAllLifestyleActivity().get(0).getId()))
                .flashAttr("lifestyleActivity", lifestyleActivity)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/see-lifestyle-activities"));
    }

    @Test
    public void checkUpdateLifestyleActivityByIdInvalid() throws Exception {
        lifestyleActivity.setActivityName(null);
        this.mockMvc.perform(post("/update-lifestyle-activity/123")
                .flashAttr("lifestyleActivity", lifestyleActivity)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(status().isOk())
                .andExpect(view().name("edit-lifestyle-activity"));
    }

    @Test
    public void checkMarkLifestyleActivityAsImportant() throws Exception {
        lifestyleActivityService.getAllLifestyleActivity().get(0).setStatus("IMPORTANT");
        this.mockMvc.perform(get("/mark-lifestyle-activity-as-important/" + Long.toString(lifestyleActivityService.getAllLifestyleActivity().get(0).getId()))
                .flashAttr("lifestyleActivity", lifestyleActivity)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/see-lifestyle-activities"));
    }

    @Test
    public void checkMarkLifestyleActivityAsFinished() throws Exception {
        lifestyleActivityService.getAllLifestyleActivity().get(0).setStatus("IMPORTANT");
        this.mockMvc.perform(get("/mark-lifestyle-activity-as-finished/" + Long.toString(lifestyleActivityService.getAllLifestyleActivity().get(0).getId()))
                .flashAttr("lifestyleActivity", lifestyleActivity)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/see-lifestyle-activities"));
    }
}
