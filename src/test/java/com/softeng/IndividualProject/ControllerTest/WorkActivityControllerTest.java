package com.softeng.IndividualProject.ControllerTest;

import com.softeng.IndividualProject.model.WorkActivity;
import com.softeng.IndividualProject.repository.WorkActivityRepo;
import com.softeng.IndividualProject.service.WorkActivityService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@SpringBootTest
@AutoConfigureMockMvc
public class WorkActivityControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Mock
    private BindingResult bindingResult;

    @Mock
    private Model model;

    @Mock
    WorkActivityRepo workActivityRepo;

    @Autowired
    WorkActivityService workActivityService;

    private static WorkActivity workActivity = new WorkActivity();
    private static String activityName = "Study";
    private static LocalDate dueDate = LocalDate.of(2020, 06, 01);
    private static LocalTime dueTime = LocalTime.of(23,55);

    @BeforeEach
    void setUp() {
        workActivity.setId(123);
        workActivity.setActivityName(activityName);
        workActivity.setDueTime(dueTime);
        workActivity.setDueDate(dueDate);
        workActivityService.addWorkActivity(workActivity);
    }

    @Test
    void testLandingPage() throws Exception {
        this.mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }

    @Test
    void testSeeWorkActivityPage() throws Exception {
        this.mockMvc.perform(get("/see-work-activities"))
                .andExpect(status().isOk())
                .andExpect(view().name("see-work-activity"));
    }

    @Test
    void testShowWorkActivityForm() throws Exception {
        this.mockMvc.perform(get("/add/work-activity"))
                .andExpect(status().isOk())
                .andExpect(view().name("add-work-activity"));
    }

    @Test
    void testIfAddWorkActivityErrorThenStayAtFormAndAddValidWorkActivity() throws Exception {
        workActivity.setActivityName(null);
        this.mockMvc.perform(post("/add-work-activity")
                .flashAttr("workActivity", workActivity)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(view().name("add-work-activity"));

        workActivity.setActivityName("Homework");
        this.mockMvc.perform(post("/add-work-activity")
                .flashAttr("workActivity", workActivity)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(view().name("redirect:/see-work-activities"));
    }

    @Test
    void checkIfShowUpdateWorkActivityFormIfIdNotFoundThenThrowException() throws Exception{
        try {
            mockMvc.perform(get("/edit-work-activity/0"))
                    .andExpect(view().name("edit-work-activity"));
        } catch (Exception e) {
            assertEquals("Request processing failed; nested exception is java.lang.IllegalArgumentException: Invalid activity id 0", e.getMessage());
        }
    }

    @Test
    void checkIfShowUpdateWorkActivityFormWithValidIdThenShowTemplate() throws Exception{
        mockMvc.perform(get("/edit-work-activity/" + Long.toString(workActivityService.getAllWorkActivity().get(0).getId()))
                .flashAttr("workActivity", workActivity)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(view().name("edit-work-activity"));
    }

    @Test
    public void testUpdateWorkActivityByIdValid() throws Exception {
        this.mockMvc.perform(post("/update-work-activity/" + Long.toString(workActivityService.getAllWorkActivity().get(0).getId()))
                .flashAttr("workActivity", workActivity)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/see-work-activities"));
    }

    @Test
    public void checkUpdateWorkActivityByIdInvalid() throws Exception {
        workActivity.setActivityName(null);
        this.mockMvc.perform(post("/update-work-activity/123")
                .flashAttr("workActivity", workActivity)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(status().isOk())
                .andExpect(view().name("edit-work-activity"));
    }

    @Test
    public void checkMarkWorkActivityAsImportant() throws Exception {
        workActivityService.getAllWorkActivity().get(0).setStatus("IMPORTANT");
        this.mockMvc.perform(get("/mark-work-activity-as-important/" + Long.toString(workActivityService.getAllWorkActivity().get(0).getId()))
                .flashAttr("workActivity", workActivity)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/see-work-activities"));
    }

    @Test
    public void checkMarkWorkActivityAsFinished() throws Exception {
        workActivityService.getAllWorkActivity().get(0).setStatus("FINISHED");
        this.mockMvc.perform(get("/mark-work-activity-as-finished/" + Long.toString(workActivityService.getAllWorkActivity().get(0).getId()))
                .flashAttr("workActivity", workActivity)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/see-work-activities"));
    }
}
