package com.softeng.IndividualProject.ServiceTest;

import com.softeng.IndividualProject.model.LifestyleActivity;
import com.softeng.IndividualProject.repository.ActivityRepo;
import com.softeng.IndividualProject.repository.LifestyleActivityRepo;
import com.softeng.IndividualProject.service.ActivityService;
import com.softeng.IndividualProject.service.LifestyleActivityService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.persistence.EntityNotFoundException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@SpringBootTest
public class LifestyleActivityServiceTest {

    @Autowired
    LifestyleActivityRepo lifestyleActivityRepo;

    @Autowired
    LifestyleActivityService lifestyleActivityService;

    @Autowired
    ActivityService activityService;

    @Autowired
    ActivityRepo activityRepo;

    private LifestyleActivity lifestyleActivity = new LifestyleActivity();

    private static String activityName = "Workout";
    private static String activityURL = "https://www.youtube.com/ChloesAddiction";

    @BeforeEach
    void setUp() {
        lifestyleActivity.setId(123);
        lifestyleActivity.setActivityName(activityName);
        lifestyleActivity.setActivityURL(activityURL);
        lifestyleActivityService.addLifestyleActivity(lifestyleActivity);
    }

    @Test
    void testSuccessfullyAddLifestyleActivity() {
        assertEquals(lifestyleActivityRepo.findAllByOrderByIdAsc().get(0).getId(),
                lifestyleActivityService.getAllLifestyleActivity().get(0).getId());
    }

    @Test
    void testGetLifestyleActivityByIdNotFound() {
        try {
            lifestyleActivityService.getLifestyleActivityById(1234);
        } catch (EntityNotFoundException ex) {
            assertNull(ex.getMessage());
        }
        try {
            lifestyleActivity.setId(1234);
            lifestyleActivityService.updateLifestyleActivity(lifestyleActivity);
        } catch (EntityNotFoundException ex) {
            assertNull(ex.getMessage());
        }
    }

    @Test
    void testGetLifestyleActivityByIdAndUpdateLifestyleActivityByIdExist() {
        LifestyleActivity la = new LifestyleActivity();
        la.setId(4);
        la.setActivityName("Kdrama");
        la.setActivityURL("https://www.netflix.com");
        lifestyleActivityService.addLifestyleActivity(la);
        try {
            assertEquals(la.getActivityName(), lifestyleActivityService.getLifestyleActivityById(la.getId()).getActivityName());
        } catch (EntityNotFoundException e) {
            assertNull(e.getMessage());
        }
        la.setId(4);
        la.setActivityName("Movies");
        la.setActivityURL("https://www.netflix.com");
        lifestyleActivityService.updateLifestyleActivity(la);
        try {
            assertEquals("Movies", lifestyleActivityService.getLifestyleActivityById(la.getId()).getActivityName());
        } catch (EntityNotFoundException e) {
            assertNull(e.getMessage());
        }
    }
}
