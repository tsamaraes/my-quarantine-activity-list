package com.softeng.IndividualProject.ServiceTest;

import com.softeng.IndividualProject.model.Activity;
import com.softeng.IndividualProject.model.LifestyleActivity;
import com.softeng.IndividualProject.model.WorkActivity;
import com.softeng.IndividualProject.repository.ActivityRepo;
import com.softeng.IndividualProject.repository.WorkActivityRepo;
import com.softeng.IndividualProject.service.ActivityService;
import com.softeng.IndividualProject.service.WorkActivityService;
import org.apache.tomcat.jni.Local;
import org.hibernate.jdbc.Work;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.persistence.EntityNotFoundException;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@SpringBootTest
public class WorkActivityServiceTest {

    @Autowired
    private WorkActivityRepo workActivityRepo;

    @Autowired
    private WorkActivityService workActivityService;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private ActivityRepo activityRepo;

    private WorkActivity workActivity = new WorkActivity();

    private static String activityName = "Study";
    private static LocalDate dueDate = LocalDate.of(2020, 6, 1);
    private static LocalTime dueTime = LocalTime.of(23,55);

    @BeforeEach
    void setUp() {
        workActivity.setId(123);
        workActivity.setActivityName(activityName);
        workActivity.setDueTime(dueTime);
        workActivity.setDueDate(dueDate);
        workActivityService.addWorkActivity(workActivity);
    }

    @Test
    void testSuccessfullyAddLWorkActivity() {
        assertEquals(workActivityRepo.findAllByOrderByDueDateAsc().get(0).getId(),
                workActivityService.getAllWorkActivity().get(0).getId());
    }

    @Test
    void testGetWorkActivityByIdNotFound() {
        try {
            workActivityService.getWorkActivityById(1234);
        } catch (EntityNotFoundException ex) {
            assertNull(ex.getMessage());
        }
        try {
            workActivity.setId(1234);
            workActivityService.updateWorkActivity(workActivity);
        } catch (EntityNotFoundException ex) {
            assertNull(ex.getMessage());
        }
    }

    @Test
    void testGetWorkActivityByIdAndUpdateWorkActivityByIdExist() {
        WorkActivity wa = new WorkActivity();
        wa.setActivityName("Homework");
        wa.setDueTime(LocalTime.of(23, 30));
        wa.setDueDate(LocalDate.of(2020, 6, 1));
        workActivityService.addWorkActivity(wa);
        assertEquals(wa.getId(), workActivityService.getWorkActivityById(wa.getId()).getId());
        wa.setId(wa.getId());
        wa.setActivityName("Movies");
        workActivityService.updateWorkActivity(wa);
        assertEquals("Movies", workActivityService.getWorkActivityById(wa.getId()).getActivityName());
    }


}
