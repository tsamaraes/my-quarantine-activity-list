package com.softeng.IndividualProject.ServiceTest;

import com.softeng.IndividualProject.model.LifestyleActivity;
import com.softeng.IndividualProject.model.WorkActivity;
import com.softeng.IndividualProject.repository.ActivityRepo;
import com.softeng.IndividualProject.service.ActivityService;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.time.LocalTime;

@SpringBootTest
public class ActivityServiceTest {

    @Autowired
    ActivityService activityService;

    @Autowired
    ActivityRepo activityRepo;

    private WorkActivity workActivity = new WorkActivity();

    @BeforeEach
    void setUp() {
        workActivity.setActivityName("do hw");
        workActivity.setDueTime(LocalTime.of(23,55));
        workActivity.setDueDate(LocalDate.of(2020, 6, 4));
    }

    @Test
    void testGetActivityByIdAndMarkAsImportant() {
        WorkActivity wa = new WorkActivity();
        wa.setActivityName("Do Assignment");
        wa.setDueDate(LocalDate.of(2020, 6, 4));
        wa.setDueTime(LocalTime.of(23,55));
        activityRepo.save(wa);
        activityService.markThisActivity(wa, "IMPORTANT", 1);
        try {
            assertEquals("IMPORTANT", wa.getStatus());
        } catch (EntityNotFoundException e) {
            assertNull(e.getMessage());
        }

        try {
            activityService.markThisActivity(workActivity, "IMPORTANT", 1);
        } catch (EntityNotFoundException e) {
            assertNull(e.getMessage());
        }
    }

    @Test
    void testGetActivityByIdAndMarkAsFinished() {
        try {
            activityService.markThisActivity(workActivity, "IMPORTANT", 1);
        } catch (EntityNotFoundException e) {
            assertNull(e.getMessage());
        }

        WorkActivity wa = new WorkActivity();
        wa.setActivityName("Do Assignments");
        wa.setDueDate(LocalDate.of(2020, 6, 4));
        wa.setDueTime(LocalTime.of(23,55));
        wa.setStatus("");
        activityRepo.save(wa);
        activityService.markThisActivity(wa, "FINISHED", 3);
        try {
            assertEquals("FINISHED", wa.getStatus());
        } catch (EntityNotFoundException e) {
            assertNull(e.getMessage());
        }
    }

    @Test
    void testGetActivityByIdAndMarkAsImportantButStatusIsImportant() {
        WorkActivity wa = new WorkActivity();
        wa.setActivityName("Do Assignment");
        wa.setDueDate(LocalDate.of(2020, 6, 4));
        wa.setDueTime(LocalTime.of(23,55));
        wa.setStatus("IMPORTANT");
        activityRepo.save(wa);
        activityService.markThisActivity(wa, "IMPORTANT", 1);
        try {
            assertEquals("", wa.getStatus());
        } catch (EntityNotFoundException e) {
            assertNull(e.getMessage());
        }

        try {
            activityService.markThisActivity(workActivity, "IMPORTANT", 1);
        } catch (EntityNotFoundException e) {
            assertNull(e.getMessage());
        }
    }

    @Test
    void testGetActivityByIdAndMarkAsFinishedButStatusIsFinished() {
        try {
            activityService.markThisActivity(workActivity, "FINISHED", 3);
        } catch (EntityNotFoundException e) {
            assertNull(e.getMessage());
        }

        WorkActivity wa = new WorkActivity();
        wa.setActivityName("Do Assignments");
        wa.setDueDate(LocalDate.of(2020, 6, 4));
        wa.setDueTime(LocalTime.of(23,55));
        wa.setStatus("FINISHED");
        activityRepo.save(wa);
        activityService.markThisActivity(wa, "FINISHED", 3);
        try {
            assertEquals("", wa.getStatus());
        } catch (EntityNotFoundException e) {
            assertNull(e.getMessage());
        }
    }
}
