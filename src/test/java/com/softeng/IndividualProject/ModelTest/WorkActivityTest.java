package com.softeng.IndividualProject.ModelTest;

import com.softeng.IndividualProject.model.WorkActivity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WorkActivityTest {
    @Autowired
    WorkActivity workActivity = new WorkActivity();

    private static String activityName = "Software Engineering Project";
    private static LocalDate dueDate = LocalDate.of(2020, 06, 8);
    private static LocalTime dueTime = LocalTime.of(23,55);

    @BeforeEach
    void setUp() {
        workActivity.setId((long)123);
        workActivity.setActivityName(activityName);
        workActivity.setDueDate(dueDate);
        workActivity.setDueTime(dueTime);
    }

    @Test
    void testWorkActivityName() {
        assertEquals(activityName, workActivity.getActivityName());
    }
    @Test
    void testWorkActivityDueDateAndDueTime() {
        assertEquals(dueDate, workActivity.getDueDate());
        assertEquals(dueTime, workActivity.getDueTime());
    }

    @Test
    void testWorkActivityId() {
        assertEquals(123, workActivity.getId());
    }
}
