package com.softeng.IndividualProject.ModelTest;

import com.softeng.IndividualProject.model.LifestyleActivity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LifestyleActivityTest {

    @Autowired
    LifestyleActivity lifestyleActivity = new LifestyleActivity();

    private static String activityName = "Workout";
    private static String activityURL = "https://www.youtube.com/ChloesAddiction";

    @BeforeEach
    void setUp() {
        lifestyleActivity.setId((long)123);
        lifestyleActivity.setActivityName(activityName);
        lifestyleActivity.setActivityURL(activityURL);
    }

    @Test
    void testLifestyleActivityName() {
        assertEquals(activityName, lifestyleActivity.getActivityName());
    }
    @Test
    void testLifestyleActivityURL() {
        assertEquals(activityURL, lifestyleActivity.getActivityURL());
    }

    @Test
    void testLifestyleActivityId() {
        assertEquals(123, lifestyleActivity.getId());
    }


}
